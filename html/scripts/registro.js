let registro = {}

function register(nombre, modulo){
    if(!registro[nombre]){
       registro[nombre] = modulo
    }
}

function requerir(nombre){
    if(registro[nombre]){
        return registro[nombre]
    }
}