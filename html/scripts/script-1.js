register('modulo1', function (){
    let nombre = 'modulo-1'

    function mostrar(str){
        console.log(str)
    }

    return {
        mostrar,
        nombre
    }
})