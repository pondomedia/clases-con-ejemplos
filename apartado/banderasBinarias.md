
Estado saltando  [0, 0, 0, 0, 1]  => 1 en decimal
Estado piso      [0, 0, 0, 1, 0]  => 2
Estado izquierda [0, 0, 1, 0, 0]  => 4


Estado Actual    [0, 0, 1, 0, 1]  => 5

# Usando operación binaria AND para enmascarar


Estado Actual    [0, 0, 1, 0, 1] &
Estado Saltando  [0, 0, 0, 0, 1]
                -----------------
                 [0, 0, 0, 0, 1] => Estado Saltando (truthy)

Estado Actual    [0, 0, 1, 0, 1] &
Estado Piso      [0, 0, 0, 1, 0]
                -----------------
                 [0, 0, 0, 0, 0] => 0 (falsy)


Estado Actual    [0, 0, 1, 0, 1] &
Estado Izquierda [0, 0, 1, 0, 0]
                -----------------
                 [0, 0, 1, 0, 0] => Estado Izquierda (truthy)

# Usando operación binaria OR para agregar un estado

Estado Actual    [0, 0, 1, 0, 1] |
Estado Piso      [0, 0, 0, 1, 0]
                -----------------
                 [0, 0, 1, 1, 1] => Nuevo Estado Actual

# Usando operación binaria NEG y AND para eliminar un estado

Estado Actual   [0, 0, 1, 1, 1] &
~Estado Piso    [1, 1, 1, 0, 1]
                -----------------
                [0, 0, 1, 0, 1] => Nuevo Estado Actual

# En javascript

let estado = MARIO_ESTADO.PISO
estado = estado & ~MARIO_ESTADO.PISO