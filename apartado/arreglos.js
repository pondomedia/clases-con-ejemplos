// Los arreglos empiezan en 0
let arr = []
let arr2 = [1,2,3]
let arr3 = [4,5,6]

let arr4 = [...arr2, ...arr3] // [1,2,3,4,5,6]

arr[0] // undefined porque no existe

arr2[0] // 1
arr2[1] // 2

let arr5 = [arr2, arr3] // [[1,2,3], [4,5,6]]
arr5[0][2] // arr2[2] => 3


let arr6 = new Array(6) // Array(6)
let arr7 = Array.from(arr5) // Clonar arreglos
let arr8 = [...arr7] //Tambien me permite clonar
Array.isArray(arr8) // true
Array.isArray("Hola Mundo") // false
let arr9 = Array.of(1,2,3) // Lo mismo que [1,2,3]

let arr10 = arr9.concat(arr6, arr7) // Es puro, lo mismo que [...arr9, ...arr6, ...arr7]
let arr11 = arr9.copyWithin() // Se copia a si mismo en un posicion y escogiendo un inicio y fin

