// Los objetos creados con el literal {} son instancias de la clase Object

let a = {
    ciudad: 'Trujillo'
};

Object.assign(a, { nombre: 'Olmo' }, { nombre: 'Ricardo', apellido:'Sifuentes'})
console.log(a)


// Lo mismo que let b = {...a, ...{nombre: 'Olmo'}, nombre: 'Ricardo', apellido: 'Sifuentes' }
let b = Object.assign({}, a, { nombre: 'Olmo' }, { nombre: 'Ricardo', apellido:'Sifuentes'})
let clonDeA = Object.assign(a)

let libro = {
    nombre: 'Harry Potter',
    autor: {
        nombre: 'J. K. Rowling'
    }
}

let libro2 = Object.assign({}, autor) // Lo mismo que { ...autor }

Object.defineProperty(a, 'tamaño', {
    configurable: false,
    enumerable: false,
    get(){

    },
    set(value){

    },
    value: 10,
    writable: false
})

let definicionDeTamaño =  {
    configurable: false,
    enumerable: false,
    get(){
        // ...
    },
    set(value){
        // ...
    },
    value: 10,
    writable: false
}

Object.defineProperties(a, {
    tamaño: definicionDeTamaño,
    altura: {}
})

Object.getOwnPropertyDescriptor(a, 'tamaño')
Object.getOwnPropertyNames(a)