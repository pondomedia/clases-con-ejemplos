let c1 = "" // Cadena vacía es falsy
let c2 = "cadena" // Comillas dobles
let c3 = 'cadena' // Comillas simples

let c4 = `cadena` // Comilla invertidas Template literal, Literal de plantilla

let c5 = c1 + c2 + c3

let c7 = `
    asdasd
  aasda
 asd
`
let persona = {
    nombre: 'Gabito'
}
let c8 = `Hola ${persona.nombre}` // 'Hola Gabito'

let multilinea =
    "Esta es una línea\n" +
    "esta es otra línea."

let especiales = '\n\t\r\\\'\"\`'

let caracter = multilinea[10]

"tags1,tags2,tags3".split(',')
"/url".startsWith('/') // true
"/url"[0] === '/' // true