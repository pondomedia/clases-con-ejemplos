class Pad extends Phaser.GameObjects.Container {
    constructor(scene, x, y){
        super(scene, x, y)

        this.padGraphic = new Phaser.GameObjects.Graphics(scene, {x: 0, y: 0, fillStyle: { 
            color: 0xffffff
        }})
        this.setSize(36, 96)
        this.padGraphic.fillRect(-this.width/2, -this.height/2, this.width, this.height)
        
        this.add(this.padGraphic)
        
        scene.add.existing(this)
        scene.physics.add.existing(this.pad, Phaser.Physics.Arcade.DYNAMIC_BODY)        
                
        this.body.setMaxVelocity(0, 400)
        this.body.setImmovable(true)
        this.body.setBounce(0)
        this.body.setCollideWorldBounds(true)
    }



    update(){    
    }
}

module.exports = Pad