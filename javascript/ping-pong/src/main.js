const Phaser = require('phaser')
const Pad = require('./Pad')
const Ball = require('./Ball')


class Scene extends Phaser.Scene {    
    create(){
        this.cursors = this.input.keyboard.createCursorKeys()
        
        this.ball = new Ball(this, 320, 240)
        this.pad = new Pad(this, 32, 240-96/2)
        
        this.physics.add.collider(this.ball, this.pad, function(ball, pad){
            ball.speed += 10
        })
    }

    update(){
        if(this.cursors.up.isDown){
            this.pad.body.setAccelerationY(-1000)
        }
        
        else if(this.cursors.down.isDown){
            this.pad.body.setAccelerationY(1000)
        }
        else {
            this.pad.body.setVelocityY(0)
            this.pad.body.setAccelerationY(0)
        }
    }
}

let game = new Phaser.Game({
    width: 640,
    height: 480,
    type: Phaser.AUTO,
    parent: 'game',
    autoFocus: true,
    title: 'ping-pong',
    physics: {
        default: 'arcade',
        arcade:{
            debug: true,
            x: 0,
            y: 0,
            width: 640,
            height: 480
        }        
    },
    scene: [ Scene ],
    
})
