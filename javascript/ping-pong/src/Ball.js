class Ball extends Phaser.GameObjects.Container {
    constructor(scene, x, y){
        super(scene, x, y)
        this.speed = 400
        scene.add.existing(this)
        scene.physics.add.existing(this, Phaser.Physics.Arcade.DYNAMIC_BODY)
        this.body.setCircle(16)
        this.ballGraphic = new Phaser.GameObjects.Graphics(scene, {x: 0, y: 0, fillStyle: { 
            color: 0xffffff
        }})
        this.ballGraphic.fillCircle(16, 16, 16)
        this.add(this.ballGraphic)
        
        let vector = new Phaser.Math.Vector2(1, 1)
        vector.setToPolar(Math.random() * 2 * Math.PI, 1)
        vector.scale(400)
        this.body.setFriction(0, 0)
        this.body.setVelocity(vector.x, vector.y)
        this.body.setBounce(1, 1)
        this.body.setCollideWorldBounds(true)
       //this.setInteractive(new Phaser.Geom.Circle(0, 0, 32), ()=>{})
    }


    preUpdate(){
        
        //console.log(this.body.velocity)
        let direction = this.body.velocity.clone().normalize()
        this.body.setVelocity(direction.x * this.speed, direction.y * this.speed)
    }
}

module.exports = Ball