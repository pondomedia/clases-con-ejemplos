// NO CAIGAS EN OPTIMIZACIÓN PREMATURA
function suma(a, b){
    return a + b
}

function duplicar(n){
    n*2
}
// Dependiendo del tipo de datos se mandan por valor o referencia
function eliminaPrimero(arr){
    arr[0] = null
}

let a = 5
let miArreglo = [1,2,3]
eliminaPrimero(miArreglo) // miArreglo => [null, 1, 2, 3]
duplicar(a)

function sumaSi(a = 0, b = 0, condicion = true){    
    // b = b || 0 // Hay que tener cuidado
    // b = b == null ? 0 : b
    // if(b == null){ b = 0 }
    
    // ESTE NO:  b ?= 0 Esta en proceso de aceptación
    
    // return condicion ? a + b : a
    if(condicion){
        return a + b
    }
    
    return a
}

// parámetro resto o rest parameter
function sumarTodo(...operandos){
    let resultado = 0
    for(let i=0; i<operandos.length; i++){
        resultado += operandos[i]
    }
    return resultado

    /*
    return operandos.reduce((anterior, actual)=>anterior + actual, 0)
    */
}

// Parámetros Nombrados
function nombreCompleto({ nombres, apellidos, enMayusculas, ordenInverso }={}){
    nombres = nombres || ""
    apellidos = apellidos || ""
    enMayusculas = enMayusculas || false
    ordenInverso = ordenInverso || false

    nombres = enMayusculas ? nombres.toUpperCase() : nombres
    apellidos = enMayusculas ? apellidos.toUpperCase() : apellidos

    if(ordenInverso){
        return apellidos + ' ' + nombres
    }

    return nombres + ' ' + apellidoss
}


let sumaTotal = sumarTodo(1,2,3,4,5,6)

let a = suma(10, 5)
let b = sumaSi(10, 5, false)
let aliasSuma = suma

// nombreCompleto('', '', true, false)
nombreCompleto({
    apellidos: '',
    nombres: '',
    enMayusculas: true,
    ordenInverso: false
});

let celular = "telefono" // "telefono", "celular", "otro"
let c = {
    direccion: 'Ciro Alegria 368',
    celular: '99999123',
    "nombres completos": "Rodrigo Carranza"
}

// higher order function
function potenciaAl(a){

    // closure 
    function potencia(b){
        return b ** a
    }

    return potencia
}

let cuadrado = potenciaAl(2)
// luego puedo llamar cuadrado(2)
let cubo = potenciaAl(3)

// higher order function
// funcionDeMapeo es una funcion que recibirá
// tres parámetro, el elemento actual, la posición y arreglo entero
function map(arregloOriginal, funcionDeMapeo){
    let nuevoArreglo = []
    for(let i=0; i<arregloOriginal.length; i++){
        let actual = arregloOriginal[i]
        let procesado = funcionDeMapeo(actual, i, arregloOriginal)
        nuevoArreglo.push(procesado)

        /*
        nuevoArreglo.push(
            funcionDeMapeo(arregloOriginal[i], i, arregloOriginal)
        )        
        */
    }

    return nuevoArreglo
}

function call(fn, ...parametros){
    return fn(...parametros)
}

function adornar(medio, functionAdorno){
    return functionAdorno(medio)
}

function secuencia(n){
    if(n===1){
        return n.toString()
    }
    return secuencia(n-1) + ' ' + n.toString()
}

function factorial(n){
    if(n===1){
        return 1
    }
    return factorial(n-1) * n
}

// ¿Qué es un función pura?