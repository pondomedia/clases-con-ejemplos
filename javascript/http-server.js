const http = require('http')


let server = new http.Server((request, response) => {
    let respuesta = 'Solicito el recurso ' + request.url + ' con el método ' + request.method
    response.write(respuesta)
    
    response.end()
})

server.listen({
    port: 3000
})

