const Phaser = require('phaser')

class Principal extends Phaser.Scene {
    constructor(){
        super('Principal')
    }
    
    preload(){
        this.load.image('mario', '/assets/mario.png')
    }
    
    create(){
        this.mario = this.add.sprite(400, 400, 'mario')
        this.piso = this.add.rectangle(400,800-16,800,32, 0xff0000)
        this.mario.setScale(0.25, 0.25)
        this.mario.setState(0)
        
        this.mario.setData('SALTO', 0)

        this.physics.add.existing(this.mario, Phaser.Physics.Arcade.DYNAMIC_BODY)
        this.physics.add.existing(this.piso, Phaser.Physics.Arcade.STATIC_BODY)
        this.physics.add.collider(this.mario, this.piso, (mario) => {
            this.mario.setData('SALTO', 0)
        })

        this.mario.body.setSize(256, 256)
        this.mario.body.setMaxVelocity(400) 

        this.upKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP)
        this.leftKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT)
        this.rightKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT)
    }

    update(){
        if(Phaser.Input.Keyboard.JustDown(this.upKey) && [0,1].includes(this.mario.getData('SALTO'))){
            this.mario.body.setVelocityY(-500)
            
            if(this.mario.getData('SALTO') === 0){
                this.mario.setData('SALTO', 1)
            }
            else if(this.mario.getData('SALTO') === 1){
                this.mario.setData('SALTO', 2)
            }
        }
        
        if(this.leftKey.isDown){
            this.mario.body.setAccelerationX(-4000)
            this.mario.setData('DIRECCION', 'IZQ')
        }
        else if(this.rightKey.isDown){
            this.mario.body.setAccelerationX(4000)
            this.mario.setData('DIRECCION', 'DER')
        }
        else if(Phaser.Input.Keyboard.JustUp(this.leftKey) ||
            Phaser.Input.Keyboard.JustUp(this.rightKey)
        ){
            this.mario.body.setAccelerationX(-this.mario.body.velocity.x*4)
        }
        
        if(this.mario.getData('DIRECCION') === 'IZQ'){
            if(this.mario.body.velocity.x > 0 ){
                this.mario.body.setVelocityX(0)
                this.mario.body.setAccelerationX(0)
            }
        }
        if(this.mario.getData('DIRECCION') === 'DER'){
            if(this.mario.body.velocity.x < 0 ){
                this.mario.body.setVelocityX(0)
                this.mario.body.setAccelerationX(0)
            }
        }
    }
}

const game = new Phaser.Game({
    type: Phaser.CANVAS,
    autoFocus: true,
    backgroundColor: 0,
    width: 800,
    height: 800,
    parent: 'juego',
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { x: 0, y: 980 },
            debug: true
        },
    },
    scene: [Principal],
})