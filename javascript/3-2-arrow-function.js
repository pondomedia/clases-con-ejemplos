// - Es una forma concisa de escribir funciones
// - También se usan para mantener el this en su ámbito
// - Son anónimas

let suma = (a,b) => {
    return a + b
}

suma = (a,b) => a + b

/* function potencia(exponente){
    return function (base){
        return base ** exponente
    }
} */
const potencia = exponente => base => base ** exponente

let cubo = n => n ** 3


[1,2,3,4,5,6,7,8,9,10].
    filter( el => el % 2 ).
    map( el => el ** 2).
    reduce((pv, cv) => cv + pv)

    