class HttpError extends Error {
    constructor(statusCode){
        super()
        this.statusCode = statusCode
    }
}

class NotFoundError extends HttpError {
    constructor(){
        super(404)        
    }
}

class BadRequestError extends HttpError {
    constructor(){
        super(400)        
    }
}

class ErrorUsuarioExistente extends Error {}
class ErrorContraseñaInsegura extends Error {
    constructor(contraseña){
        super()
        this.longitud = contraseña.length
    }
}

function crearUsuario(username, password){
    if(username === 'Olmo'){
        throw new ErrorUsuarioExistente()
    }

    if(password.length < 8 ){
        throw new ErrorContraseñaInsegura(password)
    }
}


function recibePeticion(peticion){
    try {
        crearUsuario(peticion.username, peticion.password)
    }
    catch(error){
        if(error instanceof ErrorUsuarioExistente){
            guardaBaseDeDatos('El usuario ya existe')
        }

        else if(error instanceof ErrorContraseñaInsegura){
            //return `La contraseña es insegura, debe tener más de 8 caracteres y tiene ${error.longitud}`
        }
        throw error
    }
    finally {
        // Esto se ejecuta sea que hay o no hay error
    }
}

try{
    console.log(recibePeticion({ username: 'Rodrigo', password: 'asd'}))
}
catch(e){
    console.log('hubo un error')
}

console.log(recibePeticion({ username: 'Rodrigo', password: null }))