# NPM
Npm es un programa de línea de comando (CLI) que nos ayuda a administrar un proyecto en nodejs

## Configuración para Git
Dado que npm instalará mucho archivos y no queremos que estos se incluyan en el repositorio debemos agregar la carpeta generada(node_modules) a la lista de archivos y carpetas ignoras por Git en el archivo .gitnore

Ejemplo de archivo .gitnore
```
node_modules
dist
build
.cache
```

## Comandos Básicos

`npm init`  
Para crear un proyecto o paquete de npm. Nos hará una serie de preguntar sobre nuestro proyecto como el autor, la versión, si es privado, el repositorio donde se encuentra, etc.

`npm install [--only=prod|production]`  
Ejecutar este comando sin un nombre de paquete (`npm install`) instalará todos las dependencias listadas en el archivo `package.json` listadas en la propiedades `dependencies` y `devDependencies`

El flag `--only` sirve para indicar cuales de las dependencias quieres que instalen. Pasar `prod` o `production` hará que solo se instalen las dependencias listadas en `dependencies`, estas son las dependencias de producción.


`npm install [--save-dev] [--global] <paquete>`  
Busca e instala un paquete desde el repositorio central de [npm](https://npmjs.com) y lo agrega a las dependencias de nuestro proyecto.

El flag `--save-dev` o `-D` indicará a npm que instale el paquete como dependencia de desarrollo.

El flag `--global` o `-G` hará que npm instale el paquete en el ámbito global es decir estará disponible para todo el sistema.

`npm run <comando>`  
Ejecuta un comando listado en la propiedad `scripts` en el archivo `package.json`, al momento de ejecutar un comando, npm agregará la ruta `node_modules/.bin` a la variable de entorno `PATH` del usuario o del sistema con mayor prioridad; así, al ejecutar npm run, tendremos disponibles todos los paquetes que también incluyan ejecutable para ser llamados directamente

# Bundling o Empaquetado
Es el proceso de armar un paquete final de tus recursos en los que se incluyen tus módulos de javascripts, tus archivos html, tus imágenes, hojas de estilo css.

## Programas para bundling para el navegador
- Webpack
- Rollup
- Gulp
- Browserify

# Browserify
Es un programa de línea de comandos que nos ayuda a empaquetar los módulos de node para su uso en el navegador

## Comandos de Browserify

`browserify <modulo_principal> -o <archivo_de_salida>`
Con este comando podemos compilar un módulo javascript con todas sus dependencias en un solo archivo

## Observar los cambios y recompilar con browserify
Necesitamos instalar la versión modificada de `browserify` llamada `watchify`, los parámetros que recibe son los mismos que browserify

`npm install --save-dev watchify`  

# Ejecutar un servidor local de pruebas
Para ejecutar un servidor local de pruebas podemos usar programas de línea de comando que nos provean de estars funcionalidades. Ejemplo: `serve`

`npm install --save-dev serve`  
`serve ./public`  

# Ejecutar un servidor de pruebas y observar los cambios para recompilar al mismo tiempo
Para esto requerimos un programa de línea de comandos de NPM llamado `npm-run-all`

Uso básico de npm-run-all para ejecutar dos scripts de npm en paralelo
`npm-run-all -p watch serve`