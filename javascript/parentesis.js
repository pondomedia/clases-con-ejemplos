let ruta = 'mi-libreria'

function aMayúsculas(str){
    return str.toUpperCase()
}

function dividirEnLosGuiones(str){
    return str.split('-')
}

dividirEnLosGuiones(aMayúsculas(ruta))

const aMayúsculasYLuegosDivide = str => dividirEnLosGuiones(aMayúsculas(str))

const aMayúsculasYLuegosDivide = aMayúsculas ||> dividirEnLosGuiones


