// Como agregar una función a un objeto
let mate = {
    suma: function(a,b){
        return a + b
    },
    resta(a,b){
        return a + b
    },
    multiplicar: (a,b) => a * b
}

mate.potenciar = function (a, b) {
    return a ** b
}

mate.raiz = (a, b) => Math.sqrt(a, b)

mate.valorAbsoluto = Math.abs

// Los getters y setters
// Los getters se ejecutan al acceder a un propiedad de un objeto
// Los setters se ejecutan al asignar un valor a un propiedad de un objeto
// accessors
let vector = {
    x: 10,
    y: 20,
    get tamaño(){
        return Math.sqrt(vector.x ** 2 + vector.y ** 2)
    }
}

let vector2 = {
    origen: { x: 0, y: 0 },
    fin: { x: 0, y: 0},
    set angulo(angulo){
        // va  a cambiar el fin para que coincida con el angulo
    },
    get angulo(){
        // me calcula el angulo a partir del origen y el fin
    },
    set tamaño(tam){
        
        // cambia el fin para que coincida con el tamaño
    },
    get tamaño(){
        let x0 = vector2.fin.x - vector2.origen.x
        let y0 = vector2.fin.y - vector2.origen.y

        return Math.sqrt( x0 ** 2 + y0 ** 2)
        // obtiene el tamaño a aprtide del punto origin y el fin  
    },
    get direccion(){
        let x0 = vector2.fin.x - vector2.origen.x
        let y0 = vector2.fin.y - vector2.origen.y

        let tamaño = Math.sqrt( x0 ** 2 + y0 ** 2)

        return {
            x: x0 / tamaño,
            y: y0 / tamaño
        }
    },
    set direccion({ x, y }){
        // 1. Lo hace unitario
        let tamaño = Math.sqrt( x ** 2 + y ** 2)
        let xu = x / tamaño
        let yu = y / tamaño
        // 2. Lo multiplica por el tamaño
        let nuevoVect = {
            x: xu * vector2.tamaño,
            y: yu * vector2.tamaño
        }
        // 3. Lo suma al origen
        vector2.fin = {
            x : vector2.origen.x + nuevoVect.x,
            y : vector2.origen.y + nuevoVect.y
        }
    }
}

console.log(vector2.tamaño)
vector2.fin = { x: 10, y: 20 }
vector2.direccion = { x: -20, y: -40 }

console.log("Tamaño del vector: ", vector2)

let persona = {
    primerNombre: 'Olmo',
    segundoNombre: 'Leonardo',
    set nombre(valor){
        let nombres = valor.split(' ')
        persona.primerNombre = nombres[0]
        persona.segundoNombre = nombres[1]
    },
    get nombre(){
        return persona.primerNombre + ' ' + persona.segundoNombre
    },
    apellido: 'Carranza',
    get nombreCompleto(){
        return persona.nombre + ' ' + persona.apellido
    }
}
console.log('Nombre Completo ', persona.nombreCompleto)
persona.nombre = 'Augusto Rodrigo'
console.log(persona)
console.log(persona.nombre)