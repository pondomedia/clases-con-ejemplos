'use strict';

function sumar(p2){
    return crearPunto(this.x + p2.x, this.y + p2.y)
} 


function crearPunto(x=0, y=0){
    
    return {
        x,
        y,
        sumar
    }
}

function Punto(x=0, y=0){
    // this = {}
    // this.__proto__ = Punto.prototype
    /*
        Punto.prototype = {
            constructor : Punto
        }
    */
    this.x = x
    this.y = y
}

Punto.prototype.sumar = function(p2){
    return new Punto(this.x + p2.x, this.y + p2.y)
} 


let p1_1 = crearPunto(0, 0)
let p1_2 = crearPunto(0, 0)
console.log(p1_1.sumar === p1_2.sumar)

let p2_1 = new Punto(0, 0)
let p2_2 = new Punto(0, 0)

console.log(p2_1.sumar === p2_2.sumar)

let perro = {
    nombre: 'fakis'
}


let gato = {
    nombre: 'lisandro'
}

let tortuga = {
    nombre: 'torta'
}

let prototipoDeAnimales = {
    respirar(){
        console.log('fuuu')
    },
    hablar(){
        console.log('no se hablar')
    }
}

let prototipoDePerros = {
    ladrar(){
        console.log(`${this.nombre} dice: Guau!`)
    },
    hablar(){
        this.ladrar()
    }
}

let prototipoDeGatos = {
    hablar(){
        console.log('Miauuu')
    }
}

let prototipoDeTortugas = {
    caminar(){
        console.log('voy lento')
    }
}



prototipoDePerros.__proto__ = prototipoDeAnimales
prototipoDeGatos.__proto__ = prototipoDeAnimales
prototipoDeTortugas.__proto__ = prototipoDeAnimales

perro.__proto__ = prototipoDePerros
gato.__proto__ = prototipoDeGatos
tortuga.__proto__ = prototipoDeTortugas
// Búsqueda
// objeto[propiedad] -> objeto -> objeto.__proto__ -> objeto.__proto__.__proto__ -> ...
perro.hablar()
gato.hablar()
tortuga.hablar()

console.log('\n----------------------------------------\n')
Array.prototype.map.call([1,2,3], i => i**2);
[1,2,3].map(i=>i**2)

function Animal(nombre, patas){
    this.nombre = nombre
    this.patas = patas
}

Animal.prototype.dice = function(){
    console.log(`${this.nombre} dice:`)
}
Animal.prototype.hablar = function (){
    console.log('No se hablar')
}

function Perro(nombre, raza){
    // this -> nuevo objeto creado con new Perro('Firulais')
    Animal.call(this, nombre, 4)
    this.raza = raza
}

Perro.prototype = Object.create(Animal.prototype)
Perro.prototype.constructor = Perro
Perro.prototype.dice = function(){
    // super.dice()
    Animal.prototype.dice.call(this)
    console.log('método dice de Perro')
};

Perro.prototype.hablar = function(){
    this.dice()
    console.log('Guau')
}

Perro.listarRazas = function(){
    // imprime una lista de razas
}

Perro.AÑOS_HUMANOS = 7

function Tortuga(nombre){
    Animal.call(this, nombre, 4)
}

// Tortuga.prototype.__proto__ = Animal.prototype
// Object.setPrototypeOf(Tortuga.prototype, Animal.prototype)
Tortuga.prototype = Object.create(Animal.prototype)
Tortuga.prototype.constructor = Tortuga

Tortuga.prototype.caminar = function (){
    console.log('Camino lento')
}

let dogo = new Perro('Firulais', 'Labrador')
let tortu = new Tortuga('Tortu')

dogo.hablar()
tortu.hablar()

// Perro.listarRazas()
// Perro.AÑOS_HUMANOS * dogo.edad