// Estructura de Control

// Branching o bifurcación
    // if
    // if...else
    // if..else if...else
    // switch

// Bucle
    // while
    // do...while
    // for    

// Iteración
    // for...in
    // for...of


// Estructura de Control If
if( /*expresión*/ 10 > 5){
    // Se ejecuta si es verdad la condición
}

if(10>5){
    // Se ejecuta si es verdad la condición
}
else{
    // Se ejecuta si no se cumple la condición
}

if(edad > 20){
    // Se ejecuta si es verdad la condición
}
else if(edad > 18){
    // Se ejecuta si es verdad la condición
}
else if(edad > 12){
    // Se ejecuta si es verdad la condición
}


if(edad > 20){
    // Se ejecuta si es verdad la condición
}
else if(edad > 18){
    // Se ejecuta si es verdad la condición
}
else if(edad > 12){
    // Se ejecuta si es verdad la condición
}
else {
    // Se ejecuta en última instancia si nada se cumple
}

// Estructura de Switch
switch(edad){
    case 20:
        //Ejecuta esto
        break;
    case 18:
        //Ejecuta esto
        break;
    case 16:
        //Ejecuta esto
        break;
    default:
        //Ejecuta esto
}

switch(estado){
    case "ERROR":
    case "INDETERMINADO":
        //cerrar el programa
        break;
    default:
        // ejecuta esto y continua
}

// Estructura de Control While
let a=0
while( a < 10 ){
    // Se ejecuta siempre que evalue a verdero la expresion
    a++
}

// Estructura de Control Do While
do{

}while(condicion)

// Estructura de Control For

for(/* declaracions y asignaciones */; /* condicion */; /* paso */){

}

for(let i=0; i < longitud; i++){ 
    
}

// Estructura de control For In
for(let item in iterador){

}

// Estructura de control For Of
for(let item of iterador){

}