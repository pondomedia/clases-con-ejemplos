const Phaser = require('phaser')

class Bomberman extends Phaser.GameObjects.Container {

    constructor(scene, x, y){
        super(scene, x, y)

        this.scene.add.existing(this)

        this.bomberman = this.scene.add.sprite(0, 0, 'bomberman')
        this.bomberman.play('bomberman-frente')
        this.bomberman.setScale(4, 4)

        this.add(this.bomberman)
        
    }
}

module.exports = Bomberman