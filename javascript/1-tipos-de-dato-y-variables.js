/* 
    Tipos de datos
    // Por Valor
    number => 0, -10, +10, 0.0
    string => "hola", 'hola', `hola`
    boolean => true, false
    null
    undefined
    Infinity
    Symbol
    
    //NaN Not a Number
    NaN 

    // Por referencia
    array => [ 0, "hola", true ]
    object => {
        "mi numero": 0,
        "mi mensaje": "hola",
        "un booleano": true
    }    
    
    
    functions => 
        function mi_funcion(parametro1){
            // cuerpo de la función
        }

        const mi_function = function (parametro1) {
            // cuerpo de la función
        }

        const mi_function = (parametro1) => {
            // cuerpo de la función            
        }

        const mi_function = parametro1 => {
          return valor
        }

        const mi_function = parametro1 => valor

        async ...
        
*/
// Identificador válido
// [a-zA-Z$_][0-9a-zA-Z$_]+
// miVariable1
// $foo
// __init
// _$_$_$

// Declarar un variable y luego asignar
let mensaje // Implícitamente mensaje tendrá valor `undefined`
mensaje = "Hola Mundo"

// let me permite crear un variable reasignable pero no puedo redeclararla
// SyntaxError: Identifier 'nombre' has already been declared
let nombre = "Rodrigo"
let nombre = "Olmo"

// const me permite crear un variable pero no es reasignable ni redeclarable
// Me devolverá un error: TypeError: Assignment to constant variable.
const edad = 20
edad = 30

// Operadores Matemáticos
let suma = 10 + 10 // operador de suma `+`
let resta = 10 - 10 // operador de resta `-`
let multiplicacion = 10 * 10 // operador de multiplicación `*`
let division = 10 / 10 // operador de división `/`
let potenciación = 10 ** 2 // operador potenciación `**`
let modulo = 10 % 2 // operador módulo o resto `%`
// Se pueden combinar los anterior con la asignación
// += -= *= /= %= **= 

let i = 0
i++ // Es lo mismo que decir i = i + 1, pero retorna el valor original
++i // Es lo mismo que decir i = i + 1, pero retorna el nuevo valor
// igual funcionan i-- y --i

// falsy values
  // 0, false, "", null, undefined, NaN

// Operadores Booleanos
let and = true && false // operador and
  // true && true => true
  // true && false => false
  // false && true => false
  // false && false => false

  // truthy && x => x // ejemplo: 10 && [1,2,3] => [1,2,3]
  // falsy_value && x => falsy_value // ejemplo: "" && [1,2,3] => ""

let or  = true || false // operador or
  // true || true => true
  // true || false => true
  // false || true => true
  // false || false => false

  // truthy_value || x => truthy_value // ejemplo: [1,2,3] || [] => [1,2,3]
  // falsy || x => x // ejemplo: undefined || [] => []


let a
a = a || []


// Operador Binarios
let hex = 0xFFF
let bin = 0b1100111

// bin >> 2  Operador de desplazamiento hacia la derecha
// 0b111 >> 2 => 0b001

// bin << 2  Operador de desplazamiento hacia la izquierda
// 0b111 << 2 => 0b11100

// bin & bin2 Operador de multiplicación binaria
// 0b1011 & 0b0101 => 0b0001

// bin | bin2 Operador de suma binaria
// 0b1011 | 0b0101 => 0b1111

// Operadores de Comparación
let esMayor = 10 > 5 // true solo en números
let esMenor = 10 < 5 // false solo en números
// otros, >= <= solo en números
let sonIguales = 10 == 5 // x == null
let sonEstrictamenteIguales = 10 === 5 // RECOMENDADO

let sonDiferentes = 10 != 5
let sonEstrictamenteDiferentes = 10 !== 5

// Operador de Acceso de Propiedad "."
let miObjeto = { clave: "valor" }
// miObjeto.clave, me devuelve el valor de esa propiedad(valor o referencia)

// Operador de acceso indexado "[ ]" en objetos, arreglos y cadenas
// [1,2,3][0] => 1 , me devuelve una posición de un arreglo
// "hola mundo"[1] => o, me devuelve una posición de la cadena
// objetos["clave"] => "valor", me devuelve una propiedad de un objeto

// Operador Coma ",", solo en expresiones
// Siempre devuelve el último valor a la derecha de una expresión
// let a = (10, 20, fn(), "hola", null, []) => []

// Operador ternario: condicion ? valor_verdadero : valor_falso
// let miValor = variable1 > variable2 ? "Hola" : "Adios"

