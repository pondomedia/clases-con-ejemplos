// Objetos de ejemplo
let libro = {
    titulo: "Harry Potter y la Cámara Secreta",
    autor: {
        nombre: "J.K.Rowling"
    }
}

// Los objeto son contenedores de datos
let a = {} // El objeto más pequeño
// Las claves si no van entre comillas tienen que ser identificadores válidos
let bar = "valor"
let b = {
    foo: "bar", // El lado derecho puede ser cualquier tipo dato válido
    "foo bar": "baz",
    // Si existe una variable con este nombre en su
    // alcance se crea un propiedad con el valor de esa variable
    bar // Es lo mismo que bar: bar
}

// Asignación posterior de propiedades
a.foo = "bar"
a["foo"] = "bar"
//a[variable] = "bar"
a["hola mundo"] = "ok bye" // Cuando la clave no es un identificador válido

// Obtención del valor/referencia de una propiedads
a.foo
a["foo"]
//a[variable]
a["hola mundo"] // Cuando la clave no es un identificador válido
a.bar // Si no exite la propiedad me retonar `undefined`

// Puedes obtener de forma consecutiva las propiedades más internas
libro.autor.nombre;
(((libro).autor).nombre)

try{
    libro.autor.lugarOrigen.ciudad // undefined.ciudad me va a generar un error
}
catch(e){}

let tieneCiudad = Boolean(libro && libro.autor && libro.autor.lugarOrigen && libro.autor.lugarOrigen.ciudad)
try{
    let ciudad = libro.autor.lugarOrigen.ciudad
}catch(e){}
    

// Puedo eliminar una propiedad de un objeto
delete libro.autor.nombre

// Crear objetos de forma dinámica
/**
 * @param {number} x 
 * @param {number} y 
 */
function crearPunto(x, y){
    let tamaño = Math.sqrt(x**2 + y**2)
    return { x, y, tamaño }
}

console.log(crearPunto(0,0))
console.log(crearPunto(3,4))

// Deestructuración de objetos
// Solo sirve con declaraciones con asignación inmediata de variable
let { x, y } = crearPunto(3, 4)
let { x: x1, y: y1 } = crearPunto(5, 6)

/*
let __crearPunto$1 = crearPunto(0,0)
let x = __crearPunto$1.x
let y = __crearPunto$1.y
let __crearPunto$2 = crearPunto(5,6)
let x1 = __crearPunto$2.x
let y1 = __crearPunto$2.y
*/
