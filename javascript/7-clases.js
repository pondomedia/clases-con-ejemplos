// variables y funciones son en camelCase
// clases son en CamelCase
// constantes son en SNAKE_CASE
// css y html son en kebab-case
// propiedades de objetos de apis son en snake_case

class Animal {
    constructor(nombre, edad=0, patas, tipo){
        this.nombre = nombre
        this.edad = edad
        this.patas = patas
        this.tipo = tipo
    }

    hablar(){
        console.log(`${this.nombre} dice: No se hablar`)
    }
}

class Perro extends Animal {
    constructor(nombre, edad){
        //Animal.call(this, nombre, edad, 4, 'mamífero')
        super(nombre, edad, 4, 'mamífero')        
    }
    
    get edadAñosHumanos(){
        return this.edad * this.constructor.AÑOS_HUMANOS
    }

    set edadAñosHumanos(edad){
        this.edad = Math.floor(edad / this.constructor.AÑOS_HUMANOS)
    }

    hablar(){
        console.log(`${this.nombre} dice: Guau`)
    }

    static listarRazas(){
        //...
        // acá el this no está vinculado a la instancia
        // no uses el this acá
    }

    static get AÑOS_HUMANOS (){
        return 7
    }
}

class Tortuga extends Animal {
    constructor(nombre, edad=0){
        super(nombre, edad, 4, 'reptil') 
    }
}

const tortuga = new Tortuga('Tortu', 100)
const perro = new Perro('Fakis', 12)

tortuga.hablar()
perro.hablar()
console.log(perro.edadAñosHumanos)
perro.edadAñosHumanos = 120
console.log(perro.edad)
