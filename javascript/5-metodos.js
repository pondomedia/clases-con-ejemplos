"use strict";

function mostrarNombre(objeto){
    return console.log(objeto.nombre)
}

function mostrarNombreMétodo(){
    return console.log(this.nombre)
}

let mario = {
    nombre: 'Mario'
}

let luigi = {
    nombre: 'Luigi'
}

mostrarNombre(mario)
//mostrarNombreMétodo() -> Me da error porque this es undefined
mario.verNombre = mostrarNombreMétodo
luigi.verNombre = mostrarNombreMétodo

mario.verNombre()
luigi.verNombre()

let verNombre = mario.verNombre
console.log("Es la misma función?: ", verNombre === mostrarNombreMétodo && mario.verNombre === luigi.verNombre)
// verNombre() -> Me da error porque this es undefined

let args = ['hola', 'mundo']
mostrarNombreMétodo.call(mario, ...args)
mostrarNombreMétodo.apply(mario, args)
let muestraNombreDeMario = mostrarNombreMétodo.bind(mario)


function bind(thisArg, ...boundArgs){
    // this => mostrarNombreMétodo
    let that = this
    return function (...args){
        // this => undefined
        return that.call(thisArg, ...boundArgs, ...args)
    }

    return (...args) => {
        return this.call(thisArg, ...boundArgs, ...args)
    }
}
mostrarNombreMétodo.bind2 = bind
let muestraNombreDeMario2 = mostrarNombreMétodo.bind2(mario)
console.log('muestraNombredeMario2:')
muestraNombreDeMario2()
